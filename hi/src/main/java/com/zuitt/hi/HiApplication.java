package com.zuitt.hi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HiApplication.class, args);
	}

	// Create a new method that will run when the route “api/hi” is accessed and will print the message of the user’s name.
	@GetMapping("api/hi")
	public String hi(@RequestParam(value="name", defaultValue = "world") String name) {
		return String.format("Hi, %s!", name);
	}

	// Create another method that will run when the route “api/course” is accessed and will print the user’s name and course enrolled.
	@GetMapping("api/course")
	public String course(@RequestParam(value="name") String name, @RequestParam(value="course") String course) {
		return String.format("Hi, I am %s! I am currently taking up a %s course.", name, course);
	}

}
